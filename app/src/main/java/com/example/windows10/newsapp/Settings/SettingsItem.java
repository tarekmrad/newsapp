package com.example.windows10.newsapp.Settings;

/**
 * Created by Windows 10 on 11/22/2017.
 */

public class SettingsItem {

    int settingsId;
    String settingsName;

    public SettingsItem(int settingsId, String settingsName) {
        this.settingsId = settingsId;
        this.settingsName = settingsName;
    }

    public int getSettingsId() {
        return settingsId;
    }

    public void setSettingsId(int settingsId) {
        this.settingsId = settingsId;
    }

    public String getSettingsName() {
        return settingsName;
    }

    public void setSettingsName(String settingsName) {
        this.settingsName = settingsName;
    }
}
