package com.example.windows10.newsapp.News;

/**
 * Created by Windows 10 on 11/25/2017.
 */

public enum SwipeDirection {
    all, left, right, none
}