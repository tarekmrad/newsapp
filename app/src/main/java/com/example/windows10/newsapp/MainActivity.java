package com.example.windows10.newsapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.windows10.newsapp.News.NewsType;
import com.example.windows10.newsapp.News.pagerAdapter;
import com.example.windows10.newsapp.Others.AboutActivity;
import com.example.windows10.newsapp.Settings.MySettingsActivity;
import com.example.windows10.newsapp.Users.User;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import klogi.com.RtlViewPager;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        /////////////////////////////////////////// Pager & Tabs //////////////////////////////////////////


        ArrayList<NewsType> NewsType = new ArrayList<>();
        NewsType.add(new NewsType(getResources().getString(R.string.News), getResources().getString(R.string.News_link)));
        NewsType.add(new NewsType(getResources().getString(R.string.Business), getResources().getString(R.string.Business_link)));
        NewsType.add(new NewsType(getResources().getString(R.string.Technology), getResources().getString(R.string.Technology_link)));
        NewsType.add(new NewsType(getResources().getString(R.string.Sport), getResources().getString(R.string.Sport_link)));
//        NewsType.add(new NewsType(getResources().getString(R.string.Health), getResources().getString(R.string.Health_link)));
        NewsType.add(new NewsType(getResources().getString(R.string.Arts), getResources().getString(R.string.Arts_link)));

        ViewPager viewPager = (RtlViewPager) findViewById(R.id.viewPager);
        pagerAdapter pagerAdapter = new pagerAdapter(getSupportFragmentManager(), NewsType);
        viewPager.setAdapter(pagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.TabLayout);
        tabLayout.setupWithViewPager(viewPager);

        /////////////////////////////////////////// DrawerBuilder //////////////////////////////////////////


        final AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.md_dark_background)
                .build();

        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<User> users = realm.where(User.class).findAll();
                for (User i : users)
                    accountHeader.addProfiles(new ProfileDrawerItem().withName(i.getName()).withEmail(i.getEmail()));
            }
        });

        String version = "";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Drawer drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader)
                .addDrawerItems(
                        new PrimaryDrawerItem().withIdentifier(1).withName(getResources().getString(R.string.drawer_item_home)).withIcon(R.drawable.ic_home_black_24dp).withIconTintingEnabled(true),
                        new DividerDrawerItem().withIdentifier(2),
                        new SecondaryDrawerItem().withIdentifier(3).withName(getResources().getString(R.string.action_settings)).withIcon(R.drawable.ic_settings_black_24dp).withIconTintingEnabled(true),
                        new SecondaryDrawerItem().withIdentifier(4).withName(getResources().getString(R.string.action_about)).withIcon(R.drawable.ic_help_black_24dp).withIconTintingEnabled(true)
                )
                .addStickyDrawerItems(
                        new PrimaryDrawerItem().withName(getResources().getString(R.string.drawer_item_version) + " " + version).withEnabled(false).withSelectable(false)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                        Intent intent;

                        if (position == 3) {
                            intent = new Intent(MainActivity.this, MySettingsActivity.class);
                            startActivity(intent);
                        } else if (position == 4) {
                            intent = new Intent(MainActivity.this, AboutActivity.class);
                            startActivity(intent);
                        }
                        // do something with the clicked item :D

                        return false;
                    }
                })
                .build();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Intent intent;

        //noinspection SimplifiableIfStatement
        switch (id) {
            case (R.id.action_settings):
                intent = new Intent(this, MySettingsActivity.class);
                startActivity(intent);
                break;

            case (R.id.action_about):
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }
}
