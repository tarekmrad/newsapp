package com.example.windows10.newsapp.Settings;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.windows10.newsapp.R;

import java.util.List;


/**
 * Created by Windows 10 on 11/21/2017.
 */

public class SettingsAdapter extends ArrayAdapter<SettingsItem> {

    List<SettingsItem> SettingsList;
    Context context;

    public SettingsAdapter(@NonNull Context context, @NonNull List<SettingsItem> objects) {
        super(context, R.layout.activity_my_settings, objects);

        this.SettingsList = objects;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ListItemView = inflater.inflate(R.layout.settings_list_item, parent, false);

        TextView settingsTitle = (TextView) ListItemView.findViewById(R.id.settingsTitle);
        ImageView settingsImage = (ImageView) ListItemView.findViewById(R.id.settingsImage);

        SettingsItem settingsItem = SettingsList.get(position);

        settingsTitle.setText(settingsItem.getSettingsName());

        String eng = "";
        int id;

        try {

            ///////////// Language
            if (settingsItem.getSettingsId() == 1) {
                eng = "Language";
                String images1 = eng.toLowerCase();
                id = context.getResources().getIdentifier(images1, "drawable", context.getPackageName());
                settingsImage.setImageResource(id);
            }

            /////////// User
            if (settingsItem.getSettingsId() == 2) {
                eng = "User";
                String images2 = eng.toLowerCase();
                id = context.getResources().getIdentifier(images2, "drawable", context.getPackageName());
                settingsImage.setImageResource(id);
            }
        } catch (Exception ex) {
        }

        return ListItemView;
    }

}
