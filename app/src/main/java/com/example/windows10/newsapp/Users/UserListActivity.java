package com.example.windows10.newsapp.Users;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.windows10.newsapp.MyContextWrapper;
import com.example.windows10.newsapp.News.NewsType;
import com.example.windows10.newsapp.R;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class UserListActivity extends AppCompatActivity {

    ListView usersListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.Users));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });

        usersListView = (ListView) findViewById(R.id.usersListView);

        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                final RealmResults<User> users = realm.where(User.class).findAll();
                ArrayAdapter<User> itemsAdapter = new ArrayAdapter<>(UserListActivity.this, android.R.layout.simple_list_item_1, users);
                usersListView.setAdapter(itemsAdapter);

                usersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intentUser = new Intent(UserListActivity.this, UserActivity.class);

                        User user = users.get(position);

                        RealmList<NewsType> NewsTypeUser = user.getNewsType();
                        ArrayList<String> NewsTypeStr = new ArrayList<String>();

                        for (int i = 0; i < NewsTypeUser.size(); i++)
                            NewsTypeStr.add(NewsTypeUser.get(i).getType());

                        intentUser.putExtra("userName", user.getName());
                        intentUser.putExtra("userEmail", user.getEmail());
                        intentUser.putExtra("userNewsType", NewsTypeStr);

                        startActivity(intentUser);
                    }
                });


            }
        });
    }
    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }
}
