package com.example.windows10.newsapp.News;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.windows10.newsapp.MyContextWrapper;
import com.example.windows10.newsapp.R;

public class NewsActivity extends AppCompatActivity {

    TextView newsTitle;
    TextView newsPubdate;
    TextView newsDescription;
    TextView newsLink;
    String newsLinkStr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });

        newsTitle = (TextView) findViewById(R.id.newsTitle);
        newsPubdate = (TextView) findViewById(R.id.newsPubdate);
        newsDescription = (TextView) findViewById(R.id.newsDescription);
        newsLink = (TextView) findViewById(R.id.newsLink);

        Bundle extras = getIntent().getExtras();

        String newsTitleStr = extras.getString("newsTitle");
        String newsPubdateStr = extras.getString("newsPubdate");
        String newsDescriptionStr = extras.getString("newsDescription");
        newsLinkStr = extras.getString("newsLink");


        newsTitle.setText(newsTitleStr);
        newsPubdate.append(" " + newsPubdateStr);
        newsDescription.setText(newsDescriptionStr);
        newsLink.append("\n " + newsLinkStr);

        Linkify.addLinks(newsLink, Linkify.WEB_URLS);
        newsLink.setLinkTextColor(Color.parseColor("#5A7B9A"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case (R.id.action_share):
                shareText(newsLinkStr.toString());
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void shareText(String textToShare) {


        String mimeType = "text/plain";
        String title = "Share News";

        ShareCompat.IntentBuilder
                .from(this)
                .setType(mimeType)
                .setChooserTitle(title)
                .setText(textToShare)
                .startChooser();
    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }

}
