package com.example.windows10.newsapp.Users;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.windows10.newsapp.MyContextWrapper;
import com.example.windows10.newsapp.R;

import java.util.ArrayList;

public class UserActivity extends AppCompatActivity {

    TextView userName;
    TextView userEmail;
    TextView userNewsType;
    Button deleteUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });

        userName = (TextView) findViewById(R.id.userName);
        userEmail = (TextView) findViewById(R.id.userEmail);
        userNewsType = (TextView) findViewById(R.id.userNewsType);
        deleteUser = (Button) findViewById(R.id.deleteUser);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            String userNameStr = extras.getString("userName");
            String userEmailStr = extras.getString("userEmail");
            ArrayList<String> userNewsTypestr = extras.getStringArrayList("userNewsType");


            getSupportActionBar().setTitle(userNameStr);
            userName.append(" : " + userNameStr);
            userEmail.append(" : " + userEmailStr);

            userNewsType.append(" : ");
            for (int i = 0; i < userNewsTypestr.size(); i++) {
                if (i == userNewsTypestr.size() - 1)
                    userNewsType.append(userNewsTypestr.get(i) + ".");
                else
                    userNewsType.append(userNewsTypestr.get(i) + " , ");
            }

        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }

}
