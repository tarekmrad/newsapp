package com.example.windows10.newsapp.RssClasses;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by Ammar on 17/10/2017.
 */

public class RssParser {
    private static final String TAG = "RssParser";
    RssItem item;
    ArrayList<RssItem> vectParse;
    private String urlLink;
    private RssParserListener listener;

    public RssParser(String url, RssParserListener listener) {
        this.urlLink = url;
        this.listener = listener;
    }

    public void start() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    vectParse = new ArrayList<RssItem>();

                    URL url = new URL(urlLink);
                    URLConnection con = url.openConnection();

                    System.out.println("Connection is : " + con);

                    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    System.out.println("Reader :" + reader);

                    String inputLine;
                    String fullStr = "";
                    while ((inputLine = reader.readLine()) != null)
                        fullStr = fullStr.concat(inputLine + "\n");

                    InputStream istream = url.openStream();

                    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                    Document doc = builder.parse(istream);

                    doc.getDocumentElement().normalize();


                    NodeList nList = doc.getElementsByTagName("item");

                    System.out.println();

                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Node nNode = nList.item(temp);
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element eElement = (Element) nNode;

                            String title = getTagValue("title", eElement);
                            String description = getTagValue("description", eElement);
                            String noHTMLString = description.replaceAll("\\<.*?\\>", "");
                            description = noHTMLString;
                            String link = getTagValue("link", eElement);
                            String pubdate = getTagValue("pubDate", eElement);

                            item = new RssItem(title, description, link, pubdate);
                            vectParse.add(item);
                        }
                    }

                    listener.onSuccess(vectParse);

                } catch (Exception e) {
                    listener.onFaild(e);
                }
            }
        });
        thread.start();
    }

    private String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0)
                .getChildNodes();

        Node nValue = nlList.item(0);

        return nValue.getNodeValue();

    }

    public interface RssParserListener {
        void onSuccess(List<RssItem> result);

        void onFaild(Exception e);
    }
}
