package com.example.windows10.newsapp.Users;

import com.example.windows10.newsapp.News.NewsType;

import java.io.Serializable;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class User extends RealmObject implements Serializable {

    @PrimaryKey
    private int id;
    private String name;
    private String email;
    private RealmList<NewsType> newsType;

    public User() {
        this.id = 0;
        this.name = "";
        this.email = "";
        this.newsType = null;
    }

    public User(int id, String name, String email, RealmList<NewsType> newsType) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.newsType = newsType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RealmList<NewsType> getNewsType() {
        return newsType;
    }

    public void setNewsType(RealmList<NewsType> newsType) {
        this.newsType = newsType;
    }

    public void addNewsType(NewsType newsType) {
        this.newsType.add(newsType);
    }

    @Override
    public String toString() {
        return name;
    }
}
