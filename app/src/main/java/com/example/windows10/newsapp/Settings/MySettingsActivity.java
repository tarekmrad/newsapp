package com.example.windows10.newsapp.Settings;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.windows10.newsapp.MainActivity;
import com.example.windows10.newsapp.MyContextWrapper;
import com.example.windows10.newsapp.Others.SplashScreenActivity;
import com.example.windows10.newsapp.R;
import com.example.windows10.newsapp.Users.UserListActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MySettingsActivity extends AppCompatActivity {

    private static String LANG_CURRENT = "en";

    ListView settingsListView;
    List<SettingsItem> settingsItem;
    String[] Languages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.settings));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // back button pressed
                finish();
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////

        settingsItem = new ArrayList<>();
        settingsItem.add(new SettingsItem(1, getResources().getString(R.string.Language)));
        settingsItem.add(new SettingsItem(2, getResources().getString(R.string.Users)));

        final SettingsAdapter settingsAdapter = new SettingsAdapter(this, settingsItem);
        settingsListView = (ListView) findViewById(R.id.ListViewSettings);
        settingsListView.setAdapter(settingsAdapter);

        Languages = new String[]{"English", "العربية"};

        settingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                if (settingsItem.get(position).getSettingsId() == 1) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(MySettingsActivity.this);
                    builder.setTitle(getResources().getString(R.string.selectLanguage));
                    builder.setItems(Languages, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String languageToLoad = "";

                            if (Languages[which] == "English")
                                languageToLoad = "en"; // your language

                            else if (Languages[which] == "العربية")
                                languageToLoad = "ar"; // your language


                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MySettingsActivity.this);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString("Language", languageToLoad);
                            editor.apply();

                            finish();
                            startActivity(new Intent(MySettingsActivity.this, SplashScreenActivity.class));

//                            // Change The App Language And Save it
//                            Locale locale = new Locale(languageToLoad);
//                            Locale.setDefault(locale);
//                            Configuration config = new Configuration();
//                            config.locale = locale;
//                            getBaseContext().getResources().updateConfiguration(config,
//                                    getBaseContext().getResources().getDisplayMetrics());
//
//                            SharedPreferences languagepref = getSharedPreferences("language", MODE_PRIVATE);
//                            SharedPreferences.Editor editor = languagepref.edit();
//                            editor.putString("languageToLoad", languageToLoad);
//                            editor.commit();
//
//                            // Restart The App
                        }
                    });


                    builder.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
                if (settingsItem.get(position).getSettingsId() == 2) {
                    Intent userIntent = new Intent(MySettingsActivity.this, UserListActivity.class);
                    startActivity(userIntent);
                }

            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }

}
