package com.example.windows10.newsapp.News;


import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.windows10.newsapp.R;
import com.example.windows10.newsapp.RssClasses.RssItem;
import com.example.windows10.newsapp.RssClasses.RssParser;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class NewsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LINK = "link";

    // TODO: Rename and change types of parameters
    private String link;

    public NewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * <p>
     * // @param param1 Parameter 1.
     *
     * @return A new instance of fragment NewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewsFragment newInstance(String link) {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_LINK, link);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            link = getArguments().getString(ARG_LINK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);

        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        final ListView listView = (ListView) view.findViewById(R.id.ListViewNews);

        if (!isNetworkConnected()) {

            Toast.makeText(getActivity().getBaseContext(), getString(R.string.NoInternet), Toast.LENGTH_SHORT).show();
            progressBar.setVisibility(View.GONE);

        } else {

            progressBar.setVisibility(View.VISIBLE);

            RssParser rssParser = new RssParser(link, new RssParser.RssParserListener() {
                @Override
                public void onSuccess(final List<RssItem> result) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);

                            NewsAdapter newsAdapter = new NewsAdapter(getActivity().getBaseContext(), result);
                            listView.setAdapter(newsAdapter);

                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                    Intent intentNews = new Intent(getActivity(), NewsActivity.class);

                                    RssItem player = result.get(position);

                                    intentNews.putExtra("newsTitle", player.getTitle());
                                    intentNews.putExtra("newsPubdate", player.getPubdate());
                                    intentNews.putExtra("newsDescription", player.getDescription());
                                    intentNews.putExtra("newsLink", player.getLink());

                                    startActivity(intentNews);

                                }
                            });

                        }
                    });
                }

                @Override
                public void onFaild(Exception e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);
                        }
                    });
                }
            });

            rssParser.start();
        }

        return view;
    }

    private boolean isNetworkConnected() {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            return mConnectivityManager.getActiveNetworkInfo() != null;

        } catch (NullPointerException e) {
            return false;
        }
    }

}

