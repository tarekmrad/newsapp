package com.example.windows10.newsapp.News;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.windows10.newsapp.R;
import com.example.windows10.newsapp.RssClasses.RssItem;

import java.util.List;

/**
 * Created by Windows 10 on 11/15/2017.
 */

public class NewsAdapter extends ArrayAdapter<RssItem> {

    List<RssItem> RssItemList;
    Context context;

    public NewsAdapter(@NonNull Context context, @NonNull List<RssItem> objects) {

        super(context, R.layout.list_item, objects);

        this.RssItemList = objects;
        this.context = context;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ListItemView = inflater.inflate(R.layout.list_item, parent, false);

        TextView title = (TextView) ListItemView.findViewById(R.id.title);
        TextView pubDate = (TextView) ListItemView.findViewById(R.id.pubDate);

        RssItem rssItem = RssItemList.get(position);

        title.setText(rssItem.getTitle());
        pubDate.setText(rssItem.getPubdate());

        return ListItemView;
    }
}
