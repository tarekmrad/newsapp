package com.example.windows10.newsapp.Others;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.windows10.newsapp.MyContextWrapper;

import io.realm.Realm;

/**
 * Created by Windows 10 on 10/30/2017.
 */

public class MyApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
    }


}
