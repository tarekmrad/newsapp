package com.example.windows10.newsapp.News;

import java.io.Serializable;

import io.realm.RealmObject;

/**
 * Created by Windows 10 on 11/12/2017.
 */

public class NewsType extends RealmObject implements Serializable {

    private String type;
    private String link;

    public NewsType() {

    }

    public NewsType(String type, String link) {
        this.type = type;
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return "NewsType{" +
                "type='" + type + '\'' +
                '}';
    }
}
