package com.example.windows10.newsapp.RssClasses;

/**
 * Created by obaro on 27/11/2016.
 */

public class RssItem {

    private String title;
    private String description;
    private String link;
    private String pubdate;

    public RssItem(String title, String description, String link, String pubdate) {
        setTitle(title);
        setDescription(description);
        setLink(link);
        setPubdate(pubdate);
    }

    @Override
    public String toString() {
        return title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }
}
