package com.example.windows10.newsapp.News;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by Windows 10 on 11/10/2017.
 */

public class pagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<NewsType> NewsType = new ArrayList<>();

    public pagerAdapter(FragmentManager fm, ArrayList<NewsType> NewsType) {
        super(fm);
        this.NewsType = NewsType;
    }

    @Override
    public Fragment getItem(int position) {
        return NewsFragment.newInstance(NewsType.get(position).getLink());

    }

    @Override
    public int getCount() {
        return NewsType.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return NewsType.get(position).getType();
    }
}
