package com.example.windows10.newsapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.windows10.newsapp.News.NewsType;
import com.example.windows10.newsapp.Users.User;

import io.realm.Realm;

public class SignIn_Activity extends AppCompatActivity {

    EditText userName;
    EditText userEmail;
    CheckBox checkNews;
    CheckBox checkBusiness;
    CheckBox checkTechnology;
    CheckBox checkSport;
    CheckBox checkHealth;
    CheckBox checkArts;

    Button skipButton;
    Button signUpButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        skipButton = (Button) findViewById(R.id.skip);
        signUpButton = (Button) findViewById(R.id.signUp);

        userName = (EditText) findViewById(R.id.userName);
        userEmail = (EditText) findViewById(R.id.userEmail);

        checkNews = (CheckBox) findViewById(R.id.checkbox_News);
        checkBusiness = (CheckBox) findViewById(R.id.checkbox_Business);
        checkTechnology = (CheckBox) findViewById(R.id.checkbox_Technology);
        checkSport = (CheckBox) findViewById(R.id.checkbox_Sport);
        checkHealth = (CheckBox) findViewById(R.id.checkbox_Health);
        checkArts = (CheckBox) findViewById(R.id.checkbox_Arts);

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(SignIn_Activity.this, MainActivity.class);
                startActivity(mainIntent);
                finish();
            }
        });

        ////////////////////////////////////////////////////////////////////////////////////////////

        final Realm realm = Realm.getDefaultInstance();

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {

                        long PK = realm.where(User.class).count();

                        User newUser = realm.createObject(User.class, (int) PK + 1);

                        newUser.setName(userName.getText().toString());
                        newUser.setEmail(userEmail.getText().toString());

                        if (checkNews.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("News");
                            newsType.setLink(getResources().getString(R.string.News_link));
                            newUser.addNewsType(newsType);
                        }

                        if (checkBusiness.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("Business");
                            newsType.setLink(getResources().getString(R.string.Business_link));
                            newUser.addNewsType(newsType);
                        }

                        if (checkTechnology.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("Technology");
                            newsType.setLink(getResources().getString(R.string.Technology_link));
                            newUser.addNewsType(newsType);
                        }

                        if (checkSport.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("Sport");
                            newsType.setLink(getResources().getString(R.string.Sport_link));
                            newUser.addNewsType(newsType);
                        }

                        if (checkHealth.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("Health");
                            newsType.setLink(getResources().getString(R.string.Health_link));
                            newUser.addNewsType(newsType);
                        }

                        if (checkArts.isChecked()) {
                            NewsType newsType = realm.createObject(NewsType.class);
                            newsType.setType("Arts");
                            newsType.setLink(getResources().getString(R.string.Arts_link));
                            newUser.addNewsType(newsType);
                        }

                        realm.copyToRealmOrUpdate(newUser);

                        Intent mainIntent = new Intent(SignIn_Activity.this, MainActivity.class);
                        startActivity(mainIntent);
                        finish();
                    }
                });
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(newBase);
        String LANG_CURRENT = preferences.getString("Language", "en");

        super.attachBaseContext(MyContextWrapper.wrap(newBase, LANG_CURRENT));
    }

}
